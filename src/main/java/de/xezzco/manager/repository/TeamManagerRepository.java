package de.xezzco.manager.repository;

import de.xezzco.manager.entity.TeamManager;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

/**
 * User: Xezz
 * Date: 27.10.2014
 * Time: 22:40
 */
@RepositoryRestResource(collectionResourceRel = "manager", path = "manager")
public interface TeamManagerRepository extends PagingAndSortingRepository<TeamManager, Long> {
    TeamManager findById(@Param("id") Long id);
}
