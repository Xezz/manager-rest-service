package de.xezzco.manager.repository;

import de.xezzco.manager.entity.Position;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

/**
 * User: Xezz
 * Date: 27.10.2014
 * Time: 22:38
 */
@RepositoryRestResource(collectionResourceRel = "position", path = "position")
public interface PositionRepository extends PagingAndSortingRepository<Position, Long> {
    PositionRepository findById(@Param("id") Long id);
}
