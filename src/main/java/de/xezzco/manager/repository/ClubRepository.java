package de.xezzco.manager.repository;

import de.xezzco.manager.entity.Club;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.List;

/**
 * User: Xezz
 * Date: 27.10.2014
 * Time: 20:41
 * Represents the data access layer for all the clubs
 */
@RepositoryRestResource(collectionResourceRel = "club", path = "club")
public interface ClubRepository extends PagingAndSortingRepository<Club, Long> {
    Club findById(@Param("id") Long id);

    List<Club> findByName(@Param("name")String name);

    List<Club> findByNameContaining(@Param("name") String name);
}
