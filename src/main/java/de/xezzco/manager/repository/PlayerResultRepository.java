package de.xezzco.manager.repository;

import de.xezzco.manager.entity.PlayerResult;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

/**
 * User: Xezz
 * Date: 27.10.2014
 * Time: 22:37
 */
@RepositoryRestResource(collectionResourceRel = "result", path = "result")
public interface PlayerResultRepository extends PagingAndSortingRepository<PlayerResult, Long> {
    PlayerResult findById(@Param("id") Long id);
}
