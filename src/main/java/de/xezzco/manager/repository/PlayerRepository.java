package de.xezzco.manager.repository;

import de.xezzco.manager.entity.Player;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.List;

/**
 * User: Xezz
 * Date: 26.10.2014
 * Time: 00:17
 * Represents the data access layer for all the players from the clubs
 */
@RepositoryRestResource(collectionResourceRel = "players", path = "player")
public interface PlayerRepository extends PagingAndSortingRepository<Player, Long> {
    Player findById(@Param("id") Long id);

    List<Player> findBySurName(@Param("nachname") String nachname);

    List<Player> findBySurNameAndFirstName(@Param("nachname") String nachname, @Param("vorname") String vorname);

    //List<Player> findByPosition(@Param("Position") Position pos);
}
