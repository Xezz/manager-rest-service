package de.xezzco.manager;

import de.xezzco.manager.config.RestRepoConfiguration;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

/**
 * User: Xezz
 * Date: 25.10.2014
 * Time: 23:54
 */
@Configuration
@EnableJpaRepositories
@ComponentScan
@Import(RestRepoConfiguration.class)
@EnableAutoConfiguration
public class Application {
    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }


}
