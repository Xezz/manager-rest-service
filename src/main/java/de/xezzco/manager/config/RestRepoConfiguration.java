package de.xezzco.manager.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.data.rest.core.config.RepositoryRestConfiguration;
import org.springframework.data.rest.webmvc.config.RepositoryRestMvcConfiguration;

import java.net.URI;
import java.net.URISyntaxException;

/**
 * User: Xezz
 * Date: 16.11.2014
 * Time: 13:42
 */
@Configuration
public class RestRepoConfiguration extends RepositoryRestMvcConfiguration {
    @Override
    protected void configureRepositoryRestConfiguration(RepositoryRestConfiguration config) {
        super.configureRepositoryRestConfiguration(config);
        try {
            config.setBaseUri(new URI("/managerapi"));
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
    }
}
